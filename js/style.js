class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    if (value.length == 0) {
      alert("Ім'я надто коротке.");
      return;
    }
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    if (isNaN(value) || value == "" || value < 14) {
      alert("Введіть вік");
      return;
    }
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    if (isNaN(value) || value == "" || value < 0) {
      alert("Введіть зарплату");
      return;
    }
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age);
    this.salary = salary;
    this.lang = lang;
  }
 
  set salary(value) {
    this._salary = value*3
  
  } get salary() {
    return this._salary ;
  }
}
let user = new Programmer(" Alex", "22", "30", "En");
user.name = "ben";
console.log(user);
user.age = "15";
user.salary = "200";
console.log(user);
console.log(user.salary);
console.log(user);
user.name = "Andrei";
user.age = "34";
user.salary = "120";
console.log(user.salary);
user.lang = "Uk";
console.log(user);
console.log(user.name);
console.log(user.age);
